FROM node:latest

WORKDIR /usr/src/app

COPY ./package*.json ./

RUN npm install

COPY . .

ENV NODE_ENV=production, PORT=3000

ENTRYPOINT [ "node", "./bin/www" ]

EXPOSE ${PORT}